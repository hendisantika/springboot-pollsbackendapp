package com.hendisantika.springbootpollsbackendapp.controller;

import com.hendisantika.springbootpollsbackendapp.exception.AppException;
import com.hendisantika.springbootpollsbackendapp.model.Role;
import com.hendisantika.springbootpollsbackendapp.model.RoleName;
import com.hendisantika.springbootpollsbackendapp.model.User;
import com.hendisantika.springbootpollsbackendapp.payload.ApiResponse;
import com.hendisantika.springbootpollsbackendapp.payload.JwtAuthenticationResponse;
import com.hendisantika.springbootpollsbackendapp.payload.LoginRequest;
import com.hendisantika.springbootpollsbackendapp.payload.SignUpRequest;
import com.hendisantika.springbootpollsbackendapp.repository.RoleRepository;
import com.hendisantika.springbootpollsbackendapp.repository.UserRepository;
import com.hendisantika.springbootpollsbackendapp.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-polls-backend-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/02/20
 * Time: 15.19
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.
                authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUserNameOrEmail(),
                        loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwtToken = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwtToken, "User Signed in successfully", true));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @PostMapping("/signup")
    public ResponseEntity<?> RegisterUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity(new ApiResponse(false, "User name already exists"),
                    HttpStatus.BAD_REQUEST);
        }
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity(new JwtAuthenticationResponse("",
                    "This email is registered on this plaform alredy", false), HttpStatus.BAD_REQUEST);
        }
        // Create user
        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
                signUpRequest.getPassword());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Role roles = roleRepository.findByName(RoleName.ROLE_USER).orElseThrow(() -> new
                AppException("User role not set"));
        user.setRoles(Collections.singleton(roles));
        User result = userRepository.save(user);
        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/users/{username}")
                .buildAndExpand(result.getUsername()).toUri();
        ResponseEntity.created(location).body(new ApiResponse(true,
                "User registered successfully"));
        Authentication authentication = authenticationManager.
                authenticate(new UsernamePasswordAuthenticationToken(signUpRequest.getUsername(),
                        signUpRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwtToken = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwtToken
                , "User registered successfully", true));
    }
}
