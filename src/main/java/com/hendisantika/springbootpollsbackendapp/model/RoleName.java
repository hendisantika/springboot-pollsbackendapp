package com.hendisantika.springbootpollsbackendapp.model;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-polls-backend-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/02/20
 * Time: 07.06
 */
public enum RoleName {
    ROLE_USER, ROLE_ADMIN
}