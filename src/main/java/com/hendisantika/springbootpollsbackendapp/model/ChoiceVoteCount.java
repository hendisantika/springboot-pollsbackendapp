package com.hendisantika.springbootpollsbackendapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-polls-backend-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/02/20
 * Time: 07.29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChoiceVoteCount {
    private Long choiceId;
    private Long voteCount;
}
