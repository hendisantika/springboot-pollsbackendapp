package com.hendisantika.springbootpollsbackendapp.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-polls-backend-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/02/20
 * Time: 21.29
 */
@Data
@AllArgsConstructor
public class UserIdentityAvailability {
    private Boolean available;
}
