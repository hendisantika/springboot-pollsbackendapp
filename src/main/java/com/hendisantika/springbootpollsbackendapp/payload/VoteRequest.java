package com.hendisantika.springbootpollsbackendapp.payload;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-polls-backend-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/02/20
 * Time: 18.48
 */
@Data
public class VoteRequest {
    @NotNull
    private Long choiceId;
}
