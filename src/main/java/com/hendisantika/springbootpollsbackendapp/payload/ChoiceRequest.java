package com.hendisantika.springbootpollsbackendapp.payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-polls-backend-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/02/20
 * Time: 08.04
 */
@Data
public class ChoiceRequest {
    @NotBlank
    @Size(max = 40)
    private String text;
}
