package com.hendisantika.springbootpollsbackendapp.payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-polls-backend-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/02/20
 * Time: 08.07
 */
@Data
public class LoginRequest {
    @NotBlank
    private String userNameOrEmail;

    @NotBlank
    private String password;
}
