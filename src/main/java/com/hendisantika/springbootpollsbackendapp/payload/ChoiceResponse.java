package com.hendisantika.springbootpollsbackendapp.payload;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-polls-backend-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/02/20
 * Time: 08.05
 */
@Data
public class ChoiceResponse {
    private long id;
    private String text;
    private long voteCount;
}
