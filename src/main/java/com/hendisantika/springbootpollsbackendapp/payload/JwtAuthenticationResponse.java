package com.hendisantika.springbootpollsbackendapp.payload;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-polls-backend-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/02/20
 * Time: 08.06
 */
@Data
public class JwtAuthenticationResponse {
    private String message;
    private Boolean success;
    private String accessToken;
    private String tokenType = "Bearer";

    public JwtAuthenticationResponse(String accessToken, String message, Boolean success) {
        super();
        this.accessToken = accessToken;
        this.message = message;
        this.success = success;
    }
}
